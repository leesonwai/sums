/* sums-history-row.c
 *
 * Copyright 2020-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "sums-history-row.h"

struct _SumsHistoryRow
{
  AdwActionRow  parent;

  GtkLabel     *expr_label;
  GtkLabel     *res_label;
  GtkButton    *copy_button;
};

enum
{
  PROP_0,
  PROP_EXPR,
  PROP_RES,

  N_PROPS
};

enum
{
  SIGNAL_COPY_BUTTON_CLICKED,

  N_SIGNALS
};

G_DEFINE_FINAL_TYPE (SumsHistoryRow, sums_history_row, ADW_TYPE_ACTION_ROW)

static GParamSpec *props[N_PROPS];
static guint signals[N_SIGNALS];

void
sums_history_row_set_expr (SumsHistoryRow *self,
                           const gchar    *expr)
{
  g_return_if_fail (SUMS_IS_HISTORY_ROW (self));
  g_return_if_fail (expr);

  gtk_label_set_text (self->expr_label, expr);
}

const gchar *
sums_history_row_get_expr (SumsHistoryRow *self)
{
  g_return_val_if_fail (SUMS_IS_HISTORY_ROW (self), NULL);

  return gtk_label_get_text (self->expr_label);
}

void
sums_history_row_set_res (SumsHistoryRow *self,
                          const gchar    *res)
{
  g_return_if_fail (SUMS_IS_HISTORY_ROW (self));
  g_return_if_fail (res);

  gtk_label_set_text (self->res_label, res);
}

const gchar *
sums_history_row_get_res (SumsHistoryRow *self)
{
  g_return_val_if_fail (SUMS_IS_HISTORY_ROW (self), NULL);

  return gtk_label_get_text (self->res_label);
}

static void
on_copy_button_clicked (SumsHistoryRow *self)
{
  g_signal_emit (self, signals[SIGNAL_COPY_BUTTON_CLICKED], 0, NULL);
}

SumsHistoryRow *
sums_history_row_new (void)
{
  return g_object_new (SUMS_TYPE_HISTORY_ROW, NULL);
}

static void
sums_history_row_dispose (GObject *obj)
{
  SumsHistoryRow *self = SUMS_HISTORY_ROW (obj);

  gtk_widget_dispose_template (GTK_WIDGET (self), SUMS_TYPE_HISTORY_ROW);

  G_OBJECT_CLASS (sums_history_row_parent_class)->dispose (obj);
}

static void
sums_history_row_set_property (GObject      *object,
                               guint         prop_id,
                               const GValue *value,
                               GParamSpec   *pspec)
{
  SumsHistoryRow *self = SUMS_HISTORY_ROW (object);

  switch (prop_id)
    {
    case PROP_EXPR:
      sums_history_row_set_expr (self, g_value_get_string (value));
      break;

    case PROP_RES:
      sums_history_row_set_res (self, g_value_get_string (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_history_row_get_property (GObject    *object,
                               guint       prop_id,
                               GValue     *value,
                               GParamSpec *pspec)
{
  SumsHistoryRow *self = SUMS_HISTORY_ROW (object);

  switch (prop_id)
    {
    case PROP_EXPR:
      g_value_set_string (value, sums_history_row_get_expr (self));
      break;

    case PROP_RES:
      g_value_set_string (value, sums_history_row_get_res (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_history_row_class_init (SumsHistoryRowClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  obj_class->get_property = sums_history_row_get_property;
  obj_class->set_property = sums_history_row_set_property;
  obj_class->dispose = sums_history_row_dispose;

  props[PROP_EXPR] = g_param_spec_string ("expression", NULL, NULL, NULL,
                                          G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);
  props[PROP_RES] = g_param_spec_string ("result", NULL, NULL, NULL,
                                         G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (obj_class, N_PROPS, props);

  signals[SIGNAL_COPY_BUTTON_CLICKED] = g_signal_new ("copy-button-clicked",
                                                      G_TYPE_FROM_CLASS (obj_class),
                                                      G_SIGNAL_RUN_LAST,
                                                      0, NULL, NULL, NULL,
                                                      G_TYPE_NONE, 0);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Sums/sums-history-row.ui");
  gtk_widget_class_bind_template_child (widget_class, SumsHistoryRow, expr_label);
  gtk_widget_class_bind_template_child (widget_class, SumsHistoryRow, res_label);
  gtk_widget_class_bind_template_child (widget_class, SumsHistoryRow, copy_button);

  gtk_widget_class_bind_template_callback (widget_class, on_copy_button_clicked);
}

static void
sums_history_row_init (SumsHistoryRow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
