/* sums-eval.h
 *
 * Copyright 2020-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gio/gio.h>
#include <glib-object.h>

#include "sums-enums.h"

G_BEGIN_DECLS

#define SUMS_EVAL_ERROR (sums_eval_error_quark ())
#define SUMS_TYPE_EVAL  (sums_eval_get_type ())

typedef enum
{
  SUMS_EVAL_ANGULAR_UNIT_DEGREE,
  SUMS_EVAL_ANGULAR_UNIT_RADIAN
} SumsEvalAngularUnit;

typedef enum
{
  SUMS_EVAL_ERROR_IMBALANCED_EXPRESSION,
  SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
  SUMS_EVAL_ERROR_INVALID_TOKEN,
  SUMS_EVAL_ERROR_NEGATIVE,
  SUMS_EVAL_ERROR_NON_INTEGER,
  SUMS_EVAL_ERROR_NOT_A_NUMBER,
  SUMS_EVAL_ERROR_NO_PREVIOUS_RES,
  SUMS_EVAL_ERROR_ZERO_DIVISOR
} SumsEvalError;

G_DECLARE_FINAL_TYPE (SumsEval, sums_eval, SUMS, EVAL, GObject)

void                sums_eval_cancel_flush          (SumsEval *self);

const gchar *       sums_eval_convert               (SumsEval    *self,
                                                     const gchar *token);

GQuark              sums_eval_error_quark           (void);

const gchar *       sums_eval_get_next_history      (SumsEval *self);

const gchar *       sums_eval_get_prev_history      (SumsEval *self);

gdouble             sums_eval_get_res               (SumsEval *self);

const gchar *       sums_eval_get_res_str           (SumsEval *self);

SumsEval *          sums_eval_new                   (void);

gboolean            sums_eval_parse                 (SumsEval     *self,
                                                     const gchar  *expr,
                                                     GError      **error);

void                sums_eval_queue_flush           (SumsEval *self);

G_END_DECLS
