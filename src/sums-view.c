/* sums-view.c
 *
 * Copyright 2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "sums-entry.h"
#include "sums-eval.h"
#include "sums-history-row.h"
#include "sums-view.h"

struct _SumsView
{
  GtkBox             parent;

  AdwToastOverlay   *toast_overlay;
  GtkScrolledWindow *scrolled_window;
  GtkListBox        *history_box;
  SumsEntry         *entry;
  SumsEval          *eval;
  AdwToast          *undo_toast;

  gboolean           populated;

  GSettings         *settings;
};

enum
{
  PROP_0,
  PROP_POPULATED,

  N_PROPS
};

G_DEFINE_FINAL_TYPE (SumsView, sums_view, GTK_TYPE_BOX);

static GParamSpec *props[N_PROPS];

static void
scroll_history_box (SumsView *self)
{
  gboolean computed;
  graphene_rect_t bounds;
  GtkAdjustment *adj;
  gdouble upper;

  g_assert (SUMS_IS_VIEW (self));

  computed = gtk_widget_compute_bounds (GTK_WIDGET (gtk_list_box_get_row_at_index (self->history_box, 0)),
                                        GTK_WIDGET (self->history_box),
                                        &bounds);

  g_assert (computed);

  adj = gtk_list_box_get_adjustment (self->history_box);
  upper = gtk_adjustment_get_upper (adj) + bounds.size.height;

  gtk_adjustment_set_upper (adj, upper);
  gtk_adjustment_set_value (adj, gtk_adjustment_get_upper (adj));
}

static void
set_populated (SumsView *self,
               gboolean  populated)
{
  g_assert (SUMS_IS_VIEW (self));

  populated = !!populated;

  if (self->populated == populated)
    return;

  self->populated = populated;
  if (self->populated)
    {
      gtk_widget_set_valign (GTK_WIDGET (self->history_box), GTK_ALIGN_END);
      gtk_widget_add_css_class (GTK_WIDGET (self->history_box), "boxed-list");
    }

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_POPULATED]);
}

static void
insert_history_row (SumsView       *self,
                    SumsHistoryRow *row)
{
  g_assert (SUMS_IS_VIEW (self));
  g_assert (SUMS_IS_HISTORY_ROW (row));

  if (!self->populated)
    gtk_list_box_remove_all (self->history_box);

  if (self->undo_toast)
    adw_toast_dismiss (self->undo_toast);

  set_populated (self, TRUE);
  gtk_list_box_append (self->history_box, GTK_WIDGET (row));
  scroll_history_box (self);
}

static void
on_undo_toast_dismissed (SumsView *self)
{
  g_assert (SUMS_IS_VIEW (self));

  self->undo_toast = NULL;
}

void
sums_view_clear_history (SumsView *self)
{
  g_return_if_fail (SUMS_IS_VIEW (self));

  if (!self->populated)
    return;

  self->undo_toast = adw_toast_new ("History cleared");

  adw_toast_set_button_label (self->undo_toast, "Undo");
  adw_toast_set_action_name (self->undo_toast, "toast.undo-clear");
  adw_toast_set_priority (self->undo_toast, ADW_TOAST_PRIORITY_HIGH);

  g_signal_connect_swapped (self->undo_toast, "dismissed",
                            G_CALLBACK (on_undo_toast_dismissed), self);

  adw_toast_overlay_add_toast (self->toast_overlay, self->undo_toast);

  set_populated (self, FALSE);
  sums_eval_queue_flush (self->eval);
}

static void
on_row_copy_button_clicked (SumsHistoryRow *row,
                            gpointer        user_data)
{
  SumsView *self = SUMS_VIEW (user_data);

  g_assert (SUMS_IS_VIEW (self));

  gdk_clipboard_set_text (gtk_widget_get_clipboard (GTK_WIDGET (self)),
                          sums_history_row_get_res (row));
  adw_toast_overlay_add_toast (self->toast_overlay,
                               adw_toast_new ("Result copied to clipboard"));
}

static void
undo_clear_activated (GtkWidget   *widget,
                      const gchar *action_name,
                      GVariant    *parameter)
{
  SumsView *self = SUMS_VIEW (widget);

  set_populated (self, TRUE);
  sums_eval_cancel_flush (self->eval);
}

static void
on_entry_notify_error (SumsView *self)
{
  g_assert (SUMS_IS_VIEW (self));

  adw_toast_overlay_add_toast (self->toast_overlay,
                               adw_toast_new (sums_entry_get_error (self->entry)));
}

static void
on_entry_notify_expr (SumsView *self)
{
  const gchar *expr;
  const gchar *res;
  SumsHistoryRow *row;

  g_assert (SUMS_IS_VIEW (self));

  expr = sums_entry_get_expr (self->entry);
  res = sums_eval_get_res_str (self->eval);
  row = sums_history_row_new ();

  sums_history_row_set_expr (row, expr);
  sums_history_row_set_res (row, res);

  g_signal_connect (row, "copy-button-clicked",
                    G_CALLBACK (on_row_copy_button_clicked), self);

  insert_history_row (self, row);
}

static void
on_row_activated (GtkListBox    *box,
                  GtkListBoxRow *row,
                  gpointer       user_data)
{
  const gchar *expr;
  SumsView *self = SUMS_VIEW (user_data);

  g_assert (SUMS_IS_VIEW (self));

  expr = sums_history_row_get_expr (SUMS_HISTORY_ROW (row));

  sums_entry_set_text (self->entry, expr);
  gtk_widget_grab_focus (GTK_WIDGET (self->entry));
}

static void
on_history_box_notify_visible (SumsView *self)
{
  gtk_widget_grab_focus (GTK_WIDGET (self->entry));
}

static void
on_realize (SumsView *self)
{
  gtk_widget_grab_focus (GTK_WIDGET (self->entry));
}

SumsView *
sums_view_new (void)
{
  return g_object_new (SUMS_TYPE_VIEW, NULL);
}

static void
sums_view_finalize (GObject *obj)
{
  SumsView *self = SUMS_VIEW (obj);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (sums_view_parent_class)->finalize (obj);
}

static void
sums_view_dispose (GObject *obj)
{
  SumsView *self = SUMS_VIEW (obj);

  gtk_widget_dispose_template (GTK_WIDGET (self), SUMS_TYPE_VIEW);

  G_OBJECT_CLASS (sums_view_parent_class)->dispose (obj);
}

static void
sums_view_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  switch (prop_id)
    {
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_view_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  SumsView *self = SUMS_VIEW (object);

  switch (prop_id)
    {
    case PROP_POPULATED:
      g_value_set_boolean (value, self->populated);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_view_constructed (GObject *obj)
{
  SumsView *self = SUMS_VIEW (obj);
  GtkAdjustment *adj;

  adj = gtk_scrolled_window_get_vadjustment (self->scrolled_window);

  /* Because an AdwClamp sits between the GtkScrolledWindow and the GtkListBox,
   * we have to ensure to connect the two here.
   */
  gtk_list_box_set_adjustment (self->history_box, adj);

  G_OBJECT_CLASS (sums_view_parent_class)->constructed (obj);
}

static void
sums_view_class_init (SumsViewClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  obj_class->constructed = sums_view_constructed;
  obj_class->get_property = sums_view_get_property;
  obj_class->set_property = sums_view_set_property;
  obj_class->dispose = sums_view_dispose;
  obj_class->finalize = sums_view_finalize;

  props[PROP_POPULATED] = g_param_spec_boolean ("populated", NULL, NULL, TRUE,
                                                G_PARAM_READABLE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (obj_class, N_PROPS, props);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Sums/sums-view.ui");

  gtk_widget_class_bind_template_child (widget_class, SumsView, toast_overlay);
  gtk_widget_class_bind_template_child (widget_class, SumsView, scrolled_window);
  gtk_widget_class_bind_template_child (widget_class, SumsView, history_box);
  gtk_widget_class_bind_template_child (widget_class, SumsView, entry);
  gtk_widget_class_bind_template_child (widget_class, SumsView, eval);

  gtk_widget_class_bind_template_callback (widget_class, on_realize);
  gtk_widget_class_bind_template_callback (widget_class, on_history_box_notify_visible);
  gtk_widget_class_bind_template_callback (widget_class, on_row_activated);
  gtk_widget_class_bind_template_callback (widget_class, on_entry_notify_expr);
  gtk_widget_class_bind_template_callback (widget_class, on_entry_notify_error);

  gtk_widget_class_install_action (widget_class, "toast.undo-clear", NULL, undo_clear_activated);

  g_type_ensure (SUMS_TYPE_ENTRY);
}

static void
sums_view_init (SumsView *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("io.gitlab.leesonwai.Sums");

  g_settings_bind (self->settings, "angular-unit",
                   self->eval, "angular-unit",
                   G_SETTINGS_BIND_GET);
}
