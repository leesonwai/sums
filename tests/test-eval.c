/* test-eval.c
 *
 * Copyright 2021-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "sums-eval.h"

#define EPSILON 0.00000001

static void
test_tangent (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  g_object_set (eval, "angular-unit", SUMS_EVAL_ANGULAR_UNIT_RADIAN, NULL);

  sums_eval_parse (eval, "4 tan", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 1.15782128, EPSILON);

  g_object_set (eval, "angular-unit", SUMS_EVAL_ANGULAR_UNIT_DEGREE, NULL);

  sums_eval_parse (eval, "16 tan", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 0.28674539, EPSILON);
}

static void
test_sum (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "1 1 2 3 -5 8 11 sum", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 21);
}

static void
test_sub (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "122 144 -", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, -22);
}

static void
test_sqroot (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "25 sqrt", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 5);
}

static void
test_sine (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  g_object_set (eval, "angular-unit", SUMS_EVAL_ANGULAR_UNIT_RADIAN, NULL);

  sums_eval_parse (eval, "1.6 sin", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 0.99957360, EPSILON);

  g_object_set (eval, "angular-unit", SUMS_EVAL_ANGULAR_UNIT_DEGREE, NULL);

  sums_eval_parse (eval, "128 sin", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 0.78801075, EPSILON);
}

static void
test_power (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "2 8 ^", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 256);

  sums_eval_parse (eval, "-2 10 ^", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 1024);

  sums_eval_parse (eval, "2 -2 ^", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 0.25, EPSILON);
}

static void
test_parse (void)
{
  g_autoptr (SumsEval) eval = NULL;
  g_autoptr (GError) error = NULL;

  eval = sums_eval_new ();

  g_assert_false (sums_eval_parse (eval, "", NULL));
  g_assert_true (sums_eval_parse (eval, "1 1 +", NULL));

  sums_eval_parse (eval, "1 *", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS);

  g_clear_error (&error);

  sums_eval_parse (eval, "1 2 3 +", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_IMBALANCED_EXPRESSION);

  g_clear_error (&error);

  sums_eval_parse (eval, "4 8 $", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_INVALID_TOKEN);
}

static void
test_mult (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "33.3 42 *", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 1398.6, EPSILON);

  sums_eval_parse (eval, "-12 10 *", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, -120);
}

static void
test_mod (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "13 3 mod", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 1);

  sums_eval_parse (eval, "-13 3 mod", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, -1);

  sums_eval_parse (eval, "600 19 %", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 11);
}

static void
test_log2 (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "25.6 log2", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 4.67807190, EPSILON);
}

static void
test_log10 (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "64 log10", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 1.80617997, EPSILON);
}

static void
test_log (void)
{
  g_autoptr (SumsEval) eval = NULL;
  g_autoptr (GError) error = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "128 log", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), 4.85203026, EPSILON);

  sums_eval_parse (eval, "-42 log", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_NEGATIVE);
}

static void
test_history_flush (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "2 2 +", NULL);
  sums_eval_parse (eval, "4 4 +", NULL);

  g_assert_cmpstr (sums_eval_get_prev_history (eval), ==, "4 4 +");
  g_assert_cmpstr (sums_eval_get_prev_history (eval), ==, "2 2 +");

  sums_eval_queue_flush (eval);

  sums_eval_parse (eval, "2 8 ^", NULL);
  g_assert_cmpstr (sums_eval_get_prev_history (eval), ==, "2 8 ^");
  g_assert_cmpstr (sums_eval_get_prev_history (eval), ==, "2 8 ^");

  sums_eval_queue_flush (eval);
  sums_eval_cancel_flush (eval);

  sums_eval_parse (eval, "4 16 ^", NULL);
  g_assert_cmpstr (sums_eval_get_prev_history (eval), ==, "4 16 ^");
  g_assert_cmpstr (sums_eval_get_prev_history (eval), ==, "2 8 ^");
}

static void
test_get_str (void)
{
  g_autoptr (SumsEval) eval = NULL;
  const gchar *res;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "42 !", NULL);

  res = sums_eval_get_res_str (eval);

  g_assert_cmpstr (res, ==, "1.405006118e+51");
}

static void
test_fac (void)
{
  g_autoptr (SumsEval) eval = NULL;
  g_autoptr (GError) error = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "6 !", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 720);

  sums_eval_parse (eval, "-7 !", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_NEGATIVE);

  g_clear_error (&error);

  sums_eval_parse (eval, "3.14 !", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_NON_INTEGER);
}

static void
test_divide (void)
{
  g_autoptr (SumsEval) eval = NULL;
  g_autoptr (GError) error = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "16 2 /", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 8);

  sums_eval_parse (eval, "8 0 /", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_ZERO_DIVISOR);
}

static void
test_cosine (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  g_object_set (eval, "angular-unit", SUMS_EVAL_ANGULAR_UNIT_RADIAN, NULL);

  sums_eval_parse (eval, "3.2 cos", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), -0.99829478, EPSILON);

  g_object_set (eval, "angular-unit", SUMS_EVAL_ANGULAR_UNIT_DEGREE, NULL);

  sums_eval_parse (eval, "254 cos", NULL);
  g_assert_cmpfloat_with_epsilon (sums_eval_get_res (eval), -0.27563736, EPSILON);
}

static void
test_ans (void)
{
  g_autoptr (SumsEval) eval = NULL;
  g_autoptr (GError) error = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "ans 128 *", &error);
  g_assert_error (error, SUMS_EVAL_ERROR, SUMS_EVAL_ERROR_NO_PREVIOUS_RES);

  sums_eval_parse (eval, "2 2 +", NULL);
  sums_eval_parse (eval, "38 ans +", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 42);
}

static void
test_add (void)
{
  g_autoptr (SumsEval) eval = NULL;

  eval = sums_eval_new ();

  sums_eval_parse (eval, "7 102 +", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, 109);

  sums_eval_parse (eval, "-140 12 +", NULL);
  g_assert_cmpint (sums_eval_get_res (eval), ==, -128);
}

gint
main (gint    argc,
      gchar **argv)
{
  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/eval/test-add", test_add);
  g_test_add_func ("/eval/test-ans", test_ans);
  g_test_add_func ("/eval/test-cosine", test_cosine);
  g_test_add_func ("/eval/test-divide", test_divide);
  g_test_add_func ("/eval/test-fac", test_fac);
  g_test_add_func ("/eval/test-get-str", test_get_str);
  g_test_add_func ("/eval/test-history-flush", test_history_flush);
  g_test_add_func ("/eval/test-log", test_log);
  g_test_add_func ("/eval/test-log10", test_log10);
  g_test_add_func ("/eval/test-log2", test_log2);
  g_test_add_func ("/eval/test-mod", test_mod);
  g_test_add_func ("/eval/test-mult", test_mult);
  g_test_add_func ("/eval/test-parse", test_parse);
  g_test_add_func ("/eval/test-power", test_power);
  g_test_add_func ("/eval/test-sine", test_sine);
  g_test_add_func ("/eval/test-sqroot", test_sqroot);
  g_test_add_func ("/eval/test-sub", test_sub);
  g_test_add_func ("/eval/test-sum", test_sum);
  g_test_add_func ("/eval/test-tangent", test_tangent);

  return g_test_run ();
}
