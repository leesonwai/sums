/* sums-application.c
 *
 * Copyright 2020-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "sums-application.h"
#include "sums-window.h"

struct _SumsApplication
{
  AdwApplication  parent;

  GSettings      *settings;
};

G_DEFINE_FINAL_TYPE (SumsApplication, sums_application, ADW_TYPE_APPLICATION)

#define ADD_ACCEL(app,action,...) { \
                                    const gchar *tmp[] = {__VA_ARGS__, NULL}; \
                                    gtk_application_set_accels_for_action (GTK_APPLICATION (app), \
                                                                           action, \
                                                                           tmp); \
                                  }

static gboolean show_version;

static void
angular_units_changed (GSimpleAction *simple,
                       GVariant      *parameter,
                       gpointer       user_data)
{
  SumsApplication *self = SUMS_APPLICATION (user_data);
  const gchar *unit;

  g_assert (SUMS_IS_APPLICATION (self));

  unit = g_variant_get_string (parameter, NULL);

  g_simple_action_set_state (simple, parameter);
  g_settings_set_string (self->settings, "angular-unit", unit);
}

static void
quit_activated (GSimpleAction *simple,
                GVariant      *parameter,
                gpointer       user_data)
{
  GtkApplication *self = GTK_APPLICATION (user_data);

  g_assert (GTK_IS_APPLICATION (self));

  g_application_quit (G_APPLICATION (self));
}

static void
about_activated (GSimpleAction *simple,
                 GVariant      *parameter,
                 gpointer       user_data)
{
  AdwDialog *dialog;
  GtkApplication *self = GTK_APPLICATION (user_data);
  const gchar *artists[] =
  {
    "Jakub Steiner",
    "Sam Hewitt",
    NULL
  };
  const gchar *developers[] =
  {
    "Joshua Lee <lee.son.wai@gmail.com>",
    NULL
  };

  g_assert (GTK_IS_APPLICATION (self));

  dialog = adw_about_dialog_new_from_appdata ("/io/gitlab/leesonwai/Sums/metainfo", NULL);
  adw_about_dialog_set_application_icon (ADW_ABOUT_DIALOG (dialog), ICON_NAME);
  adw_about_dialog_set_artists (ADW_ABOUT_DIALOG (dialog), artists);
  adw_about_dialog_set_copyright (ADW_ABOUT_DIALOG (dialog), "© 2020–2024 Joshua Lee");
  adw_about_dialog_set_developers (ADW_ABOUT_DIALOG (dialog), developers);

  adw_dialog_present (dialog, GTK_WIDGET (gtk_application_get_active_window (self)));
}

SumsApplication *
sums_application_new (void)
{
  return g_object_new (SUMS_TYPE_APPLICATION,
                       "application-id", APP_ID,
                       "resource-base-path", "/io/gitlab/leesonwai/Sums",
                       NULL);
}

static void
sums_application_activate (GApplication *app)
{
  GtkWindow *window;
  SumsApplication *self = SUMS_APPLICATION (app);

  g_assert (SUMS_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  if (!window)
    window = GTK_WINDOW (sums_window_new (self));

  gtk_window_present (window);
}

static void
sums_application_startup (GApplication *app)
{
  const GActionEntry app_entries[] =
  {
    {.name = "about", .activate = about_activated},
    {.name = "quit", .activate = quit_activated},
    {.name = "angular_unit", .parameter_type = "s", .state = "'radian'", .change_state = angular_units_changed}
  };
  SumsApplication *self = SUMS_APPLICATION (app);
  GAction *action;
  GVariant *variant;

  g_assert (SUMS_IS_APPLICATION (self));

  g_action_map_add_action_entries (G_ACTION_MAP (app),
                                   app_entries,
                                   G_N_ELEMENTS (app_entries),
                                   app);

  ADD_ACCEL (GTK_APPLICATION (app), "app.quit", "<Ctrl>Q");

  action = g_action_map_lookup_action (G_ACTION_MAP (app), "angular_unit");
  variant = g_variant_new_string (g_settings_get_string (self->settings, "angular-unit"));
  g_simple_action_set_state (G_SIMPLE_ACTION (action), variant);

  G_APPLICATION_CLASS (sums_application_parent_class)->startup (app);
}

static gint
sums_application_handle_local_options (GApplication *app,
                                       GVariantDict *options)
{
  if (show_version)
    {
      g_print ("Sums %s\n", VERSION);

      return 0;
    }

  return -1;
}

static void
sums_application_finalize (GObject *obj)
{
  SumsApplication *self = SUMS_APPLICATION (obj);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (sums_application_parent_class)->finalize (obj);
}

static void
sums_application_class_init (SumsApplicationClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  obj_class->finalize = sums_application_finalize;

  app_class->handle_local_options = sums_application_handle_local_options;
  app_class->startup = sums_application_startup;
  app_class->activate = sums_application_activate;
}

static void
sums_application_init (SumsApplication *self)
{
  const GOptionEntry main_entries[] =
  {
    {"version", 'V', 0, G_OPTION_ARG_NONE, &show_version, "Print version information and exit", NULL},
    {NULL}
  };

  self->settings = g_settings_new ("io.gitlab.leesonwai.Sums");

  g_application_add_main_option_entries (G_APPLICATION (self), main_entries);
}
