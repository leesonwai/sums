<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2020-2024 Joshua Lee <lee.son.wai@gmail.com> -->
<component type="desktop-application">
  <project_license>GPL-3.0-or-later</project_license>
  <metadata_license>CC0-1.0</metadata_license>
  <id>@app_id@</id>
  <launchable type="desktop-id">@app_id@.desktop</launchable>
  <name>Sums</name>
  <summary>Calculate with postfix notation</summary>
  <developer id="io.gitlab.leesonwai">
    <name>Joshua Lee</name>
  </developer>
  <update_contact>lee.son.wai@gmail.com</update_contact>
  <url type="bugtracker">https://gitlab.com/leesonwai/sums/-/issues/new</url>
  <url type="homepage">https://gitlab.com/leesonwai/sums</url>
  <content_rating type="oars-1.1"></content_rating>
  <categories>
    <category>Utility</category>
    <category>GNOME</category>
    <category>GTK</category>
  </categories>
  <keywords>
    <keyword>calculator</keyword>
    <keyword>math</keyword>
    <keyword>education</keyword>
    <keyword>science</keyword>
  </keywords>
  <branding>
    <color type="primary" scheme_preference="light">#feba4f</color>
    <color type="primary" scheme_preference="dark">#003555</color>
  </branding>
  <description>
    <p>
      Sums is a postfix calculator designed for quick calculations and focused
      on simplicity. Features include:
    </p>
    <ul>
      <li>A selection of core mathematical operations and constants</li>
      <li>Two-way history navigation</li>
      <li>Copying your results with one click</li>
    </ul>
  </description>
  <screenshots>
    <screenshot type="default">
      <caption>Start typing and get calculating</caption>
      <image height="682" width="662">https://gitlab.com/leesonwai/sums/-/raw/0.13/data/screenshots/sums-new.png</image>
    </screenshot>
    <screenshot>
      <caption>Review, re-calculate and copy your results</caption>
      <image height="682" width="662">https://gitlab.com/leesonwai/sums/-/raw/0.13/data/screenshots/sums-busy.png</image>
    </screenshot>
  </screenshots>
  <releases>
    <release version="0.14" date="2024-12-12">
      <description>
        <p>
          The latest release of Sums fixes the keyboard shortcut to clear your
          calculation history. It's now <code>Shift-Control-C</code>.
        </p>
      </description>
    </release>
    <release version="0.13" date="2024-11-09">
      <description>
        <p>
          A new release of Sums to fix a crash that can happen after clearing
          your history.
        </p>
      </description>
    </release>
    <release version="0.12" date="2024-11-02">
      <description>
        <p>
          Sums has incremented again! Now, you can clear your calculation
          history with the click of a button.
        </p>
      </description>
    </release>
    <release version="0.11" date="2024-04-25">
      <description>
        <p>
          A brand new release of Sums has arrived, featuring a new
          shortcuts window.
        </p>
      </description>
    </release>
    <release version="0.10" date="2023-09-28">
      <description>
        <p>
          The latest release of Sums is here, keeping pace with the
          latest platform technologies.
        </p>
        <p>
          Sums will now remember and restore the app window as you left
          it.
        </p>
      </description>
    </release>
    <release version="0.9" date="2023-01-02">
      <description>
        <p>
          A new year, a new release. Sums now sports a refreshed about window.
        </p>
      </description>
    </release>
    <release version="0.8" date="2022-03-26">
      <description>
        <p>
          The latest release of Sums is here, arriving with new features
          and a refreshed look built on the latest Adwaita styling! Here's
          some of the things to look forward to:
        </p>
        <ul>
          <li>Calculate at night with support for GNOME's dark mode preference</li>
          <li>New notifications that inform better and distract less</li>
          <li>A streamlined UI that gives more space for your sums</li>
          <li>Modulo operations</li>
        </ul>
      </description>
    </release>
    <release version="0.7" date="2021-03-30">
      <description>
        <p>
          Sums 0.7 is a small release that fixes the build process.
        </p>
      </description>
    </release>
    <release version="0.6" date="2021-03-29">
      <description>
        <p>
          Sums 0.6 is the latest release of the simple postfix calculator for
          your GNOME desktop. The highlights in this release include:
        </p>
        <ul>
          <li>Logarithms</li>
          <li>Fewer bugs, fewer crashes!</li>
        </ul>
      </description>
    </release>
    <release version="0.5" date="2021-03-16">
      <description>
        <p>
          Sums 0.5 is the latest release on the way to 1.0 and features a
          refreshed UI built on the latest version of GTK. New features include:
        </p>
        <ul>
          <li>Migration to the latest major release of GTK!</li>
          <li>A refreshed UI that follows the latest GNOME design patterns</li>
          <li>Copy results with the click of a button</li>
          <li>More testing and increased reliability</li>
        </ul>
      </description>
    </release>
    <release version="0.4" date="2020-12-24">
      <description>
        <p>
          Sums 0.4 is the latest incremental release on the way to 1.0.
        </p>
        <ul>
          <li>More precision displayed in results</li>
          <li>Improved history layout for a cleaner look</li>
          <li>Up and down arrow keys now cycle through your expression history</li>
          <li>Calculate square roots, powers and basic trigonometric functions</li>
          <li>Switch between working with degrees and radians</li>
          <li>Use your previous answer in calculations</li>
          <li>Less crashes, cleaner code!</li>
        </ul>
      </description>
    </release>
    <release version="0.3" date="2020-12-11">
      <description>
        <p>
          Sums 0.3 is the latest incremental release on the way to 1.0.
        </p>
        <ul>
          <li>Sums now tells you about errors in your expressions!</li>
          <li>Sums learned about its first constants, pi and e</li>
          <li>You can now calculate factorials and do summations</li>
        </ul>
      </description>
    </release>
    <release version="0.2" date="2020-11-26">
      <description>
        <p>
          Sums continues taking shape for its 1.0 release and has undergone a
          redesign, adding some convenient UI features along the way. These
          include:
        </p>
        <ul>
          <li>A more compact interface</li>
          <li>An overview of your expression history</li>
          <li>You can repeat previous expressions by clicking on them</li>
          <li>A new empty state</li>
        </ul>
      </description>
    </release>
    <release version="0.1" date="2020-11-16">
      <description>
        <p>
          0.1 is the first release of Sums and contains only the most basic of
          features.
        </p>
      </description>
    </release>
  </releases>
</component>
