/* sums-eval.c
 *
 * Copyright 2020-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <mpfr.h>

#include "sums-eval.h"

#define PREC 10

struct _SumsEval
{
  GObject      parent;

  mpfr_t       res;
  gchar       *res_str;
  GArray      *stack;
  GList       *history;

  guint        angular_unit;
  guint        stack_pos;

  gboolean     flush_set;
};

enum
{
  PROP_0,
  PROP_ANGULAR_UNIT,

  N_PROPS
};

G_DEFINE_FINAL_TYPE (SumsEval, sums_eval, G_TYPE_OBJECT);
G_DEFINE_QUARK (SumsEvalError, sums_eval_error);

enum
{
  LOG_BASE_10,
  LOG_BASE_2,
  LOG_BASE_E
};

enum
{
  TOKEN_ADD,
  TOKEN_ANS,
  TOKEN_COS,
  TOKEN_DIV,
  TOKEN_E,
  TOKEN_FAC,
  TOKEN_LOG,
  TOKEN_LOG10,
  TOKEN_LOG2,
  TOKEN_MOD,
  TOKEN_MULT,
  TOKEN_PI,
  TOKEN_POW,
  TOKEN_SIN,
  TOKEN_SQRT,
  TOKEN_SUB,
  TOKEN_SUM,
  TOKEN_TAN,

  N_TOKENS
};

static void push (SumsEval *self,
                  mpfr_t    op);

static GParamSpec *props[N_PROPS];
static struct
{
  const gchar *op;
  const gchar *notation;
} const tokens[] =
{
  {"+", "+"},
  {"ans", "ans"},
  {"cos", "cos"},
  {"/", "÷"},
  {"e", "e"},
  {"!", "!"},
  {"log", "log"},
  {"log10", "log10"},
  {"log2", "log2"},
  {"mod", "%"},
  {"*", "×"},
  {"pi", "π"},
  {"^", "^"},
  {"sin", "sin"},
  {"sqrt", "√"},
  {"-", "−"},
  {"sum", "Σ"},
  {"tan", "tan"}
};

static void
degrees_to_radians (mpfr_t   rads,
                    mpfr_ptr degs)
{
  mpfr_t pi;

  mpfr_init (pi);
  mpfr_const_pi (pi, MPFR_RNDN);
  mpfr_mul (rads, degs, pi, MPFR_RNDN);
  mpfr_div_ui (rads, rads, 180, MPFR_RNDN);
  mpfr_clear (pi);
}

static void
set_res_str (SumsEval *self)
{
  gsize len;
  GString *res;

  g_assert (SUMS_IS_EVAL (self));

  g_free (self->res_str);

  len = mpfr_snprintf (NULL, 0, "%.*Rg", PREC, self->res);
  res = g_string_new_len ("", len + 1);

  mpfr_sprintf (res->str, "%.*Rg", PREC, self->res);

  if (res->str[0] == '-')
    {
      res = g_string_erase (res, 0, 1);
      res = g_string_prepend (res, "−");
    }

  self->res_str = g_string_free_and_steal (res);
}

static mpfr_ptr
pop (SumsEval *self)
{
  g_assert (SUMS_IS_EVAL (self));
  g_assert (self->stack_pos);

  self->stack_pos--;

  return g_array_index (self->stack, mpfr_t, self->stack_pos);
}

static void
tangent (SumsEval  *self,
         mpfr_t     op,
         GError   **error)
{
  mpfr_t rads;

  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack_pos)
    {
      g_set_error (error, SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_init (rads);

  switch (self->angular_unit)
    {
    case SUMS_EVAL_ANGULAR_UNIT_DEGREE:
      degrees_to_radians (rads, pop (self));
      break;

    case SUMS_EVAL_ANGULAR_UNIT_RADIAN:
      mpfr_swap (rads, pop (self));
      break;

    default:
      g_assert_not_reached ();
    }

  mpfr_tan (op, rads, MPFR_RNDN);
  push (self, op);
  mpfr_clear (rads);
}

static void
sum (SumsEval  *self,
     mpfr_t     op,
     GError   **error)
{
  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack_pos)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_set_zero (op, 0);
  while (self->stack_pos)
    mpfr_add (op, op, pop (self), MPFR_RNDN);
  push (self, op);
}

static void
sub (SumsEval  *self,
     mpfr_t     op,
     GError   **error)
{
  g_assert (SUMS_IS_EVAL (self));

  if (self->stack_pos < 2)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_sub (op, pop (self), pop (self), MPFR_RNDN);
  push (self, op);
}

static void
sqroot (SumsEval  *self,
        mpfr_t     op,
        GError   **error)
{
  mpfr_ptr x;

  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  x = pop (self);
  if (mpfr_sgn (x) < 0)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NEGATIVE,
                   "Operand must be non-negative");
      return;
    }

  mpfr_sqrt (op, x, MPFR_RNDN);
  push (self, op);
}

static void
sine (SumsEval  *self,
      mpfr_t     op,
      GError   **error)
{
  mpfr_t rads;

  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack_pos)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_init (rads);

  switch (self->angular_unit)
    {
    case SUMS_EVAL_ANGULAR_UNIT_DEGREE:
      degrees_to_radians (rads, pop (self));
      break;

    case SUMS_EVAL_ANGULAR_UNIT_RADIAN:
      mpfr_swap (rads, pop (self));
      break;

    default:
      g_assert_not_reached ();
    }

  mpfr_sin (op, rads, MPFR_RNDN);
  push (self, op);
  mpfr_clear (rads);
}

static void
power (SumsEval  *self,
       mpfr_t     op,
       GError   **error)
{
  g_assert (SUMS_IS_EVAL (self));

  if (self->stack_pos < 2)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_pow (op, pop (self), pop (self), MPFR_RNDN);
  /* mpfr_pow() sets NaN for finite negative x and finite
   * non-integer y. Make sure not to display this to the
   * user.
   */
  if (mpfr_nan_p (op))
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NOT_A_NUMBER,
                   "Result may not be rational");
      return;
    }

  push (self, op);
}

static void
push_pi (SumsEval *self,
         mpfr_t    op)
{
  g_assert (SUMS_IS_EVAL (self));

  mpfr_const_pi (op, MPFR_RNDN);
  push (self, op);
}

static void
mult (SumsEval  *self,
      mpfr_t     op,
      GError   **error)
{
  g_assert (SUMS_IS_EVAL (self));

  if (self->stack_pos < 2)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_mul (op, pop (self), pop (self), MPFR_RNDN);
  push (self, op);
}

static void
mod (SumsEval  *self,
     mpfr_t     op,
     GError   **error)
{
  mpfr_ptr b, a;

  g_assert (SUMS_IS_EVAL (self));

  if (self->stack_pos < 2)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  b = pop (self);
  a = pop (self);
  if (!mpfr_integer_p (a))
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NON_INTEGER,
                   "%g is not an integer",
                   mpfr_get_d (a, MPFR_RNDN));
      return;
    }
  if (!mpfr_integer_p (b))
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NON_INTEGER,
                   "%g is not an integer",
                   mpfr_get_d (b, MPFR_RNDN));
      return;
    }

  mpfr_fmod (op, a, b, MPFR_RNDN);
  push (self, op);
}

static void
logarithm (SumsEval  *self,
           mpfr_t     op,
           guint      base,
           GError   **error)
{
  mpfr_ptr n;

  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack_pos)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  n = pop (self);
  if (mpfr_sgn (n) < 0)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NEGATIVE,
                   "Operand must be non-negative");
      return;
    }

  switch (base)
    {
    case LOG_BASE_10:
      mpfr_log10 (op, n, MPFR_RNDN);
      break;

    case LOG_BASE_2:
      mpfr_log2 (op, n, MPFR_RNDN);
      break;

    case LOG_BASE_E:
      mpfr_log (op, n, MPFR_RNDN);
      break;

    default:
      g_assert_not_reached ();
    }

  push (self, op);
}

static void
fac (SumsEval  *self,
     mpfr_t     op,
     GError   **error)
{
  mpfr_ptr n;

  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack_pos)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  n = pop (self);
  if (!mpfr_integer_p (n))
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NON_INTEGER,
                   "%g is not an integer",
                   mpfr_get_d (n, MPFR_RNDN));
      return;
    }
  if (mpfr_sgn (n) < 0)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NEGATIVE,
                   "Operand must be non-negative");
      return;
    }

  mpfr_fac_ui (op, mpfr_get_ui (n, MPFR_RNDN), MPFR_RNDN);
  push (self, op);
}

static void
push_e (SumsEval *self,
        mpfr_t    op)
{
  mpfr_t x;

  g_assert (SUMS_IS_EVAL (self));

  mpfr_init_set_ui (x, 1, MPFR_RNDN);
  mpfr_exp (op, x, MPFR_RNDN);
  push (self, op);
  mpfr_clear (x);
}

static void
divide (SumsEval  *self,
        mpfr_t     op,
        GError   **error)
{
  mpfr_ptr op2;

  g_assert (SUMS_IS_EVAL (self));

  if (self->stack_pos < 2)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  op2 = pop (self);
  if (!mpfr_sgn (op2))
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_ZERO_DIVISOR,
                   "Zero divisor");
      return;
    }

  mpfr_div (op, pop (self), op2, MPFR_RNDN);
  push (self, op);
}

static void
cosine (SumsEval  *self,
        mpfr_t     op,
        GError   **error)
{
  mpfr_t rads;

  g_assert (SUMS_IS_EVAL (self));

  if (!self->stack_pos)
    {
      g_set_error (error, SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_init (rads);

  switch (self->angular_unit)
    {
    case SUMS_EVAL_ANGULAR_UNIT_DEGREE:
      degrees_to_radians (rads, pop (self));
      break;

    case SUMS_EVAL_ANGULAR_UNIT_RADIAN:
      mpfr_swap (rads, pop (self));
      break;

    default:
      g_assert_not_reached ();
    }

  mpfr_cos (op, rads, MPFR_RNDN);
  push (self, op);
  mpfr_clear (rads);
}

static void
push_ans (SumsEval  *self,
          GError   **error)
{
  g_assert (SUMS_IS_EVAL (self));

  /* self->res is initialised by mpfr_init() to NaN on
   * object init and should never be set to NaN subsequently.
   * Use this to check if there's a previous result.
   */
  if (mpfr_nan_p (self->res))
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_NO_PREVIOUS_RES,
                   "No stored answer");
      return;
    }

  push (self, self->res);
}

static void
add (SumsEval  *self,
     mpfr_t     op,
     GError   **error)
{
  g_assert (SUMS_IS_EVAL (self));

  if (self->stack_pos < 2)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_INSUFFICIENT_OPERANDS,
                   "Not enough operands");
      return;
    }

  mpfr_add (op, pop (self), pop (self), MPFR_RNDN);
  push (self, op);
}

static gint
lookup_token (const gchar *token)
{
  g_assert (token);

  for (gint i = 0; i < N_TOKENS; i++)
    {
      if (!g_ascii_strcasecmp (token, tokens[i].op))
        return i;
    }

  return -1;
}

static void
push (SumsEval *self,
      mpfr_t    op)
{
  mpfr_t top;

  g_assert (SUMS_IS_EVAL (self));

  mpfr_init_set (top, op, MPFR_RNDN);
  g_array_insert_val (self->stack, self->stack_pos, top);

  self->stack_pos++;
}

static gboolean
is_number (const gchar *token)
{
  gchar *end;

  g_assert (token);

  strtod (token, &end);

  return (*end) ? FALSE : TRUE;
}

static const gchar *
get_normalised (const gchar *expr)
{
  GString *str;

  g_assert (expr);

  str = g_string_new (expr);

  for (gint i = 0; i < N_TOKENS; i++)
    g_string_replace (str, tokens[i].notation, tokens[i].op, 0);

  return g_string_free (str, FALSE);
}

static void
add_to_history (SumsEval    *self,
                const gchar *expr)
{
  g_assert (SUMS_IS_EVAL (self));
  g_assert (expr);

  if (self->flush_set)
    {
      g_list_free_full (g_steal_pointer (&self->history),
                        (GDestroyNotify) g_free);
      self->flush_set = FALSE;
    }

  self->history = g_list_prepend (self->history, g_strdup (expr));
}

void
sums_eval_cancel_flush (SumsEval *self)
{
  g_return_if_fail (SUMS_IS_EVAL (self));

  if (!self->flush_set)
    return;

  self->flush_set = FALSE;
}

const gchar *
sums_eval_convert (SumsEval    *self,
                   const gchar *token)
{
  g_return_val_if_fail (token, NULL);

  if (!g_ascii_strcasecmp (token, "ans"))
    return sums_eval_get_res_str (self);

  for (gint i = 0; i < N_TOKENS; i++)
    {
      if (!g_ascii_strcasecmp (token, tokens[i].op))
        return tokens[i].notation;
    }

  return NULL;
}

const gchar *
sums_eval_get_next_history (SumsEval *self)
{
  g_return_val_if_fail (SUMS_IS_EVAL (self), NULL);

  if (self->flush_set || !g_list_previous (self->history))
    return NULL;

  self->history = g_list_previous (self->history);

  return self->history->data;
}

const gchar *
sums_eval_get_prev_history (SumsEval *self)
{
  const gchar *prev;

  g_return_val_if_fail (SUMS_IS_EVAL (self), NULL);

  if (self->flush_set || !self->history)
    return NULL;

  prev = self->history->data;

  if (g_list_next (self->history))
    self->history = g_list_next (self->history);

  return prev;
}

gdouble
sums_eval_get_res (SumsEval *self)
{
  g_return_val_if_fail (SUMS_IS_EVAL (self), 0.0);

  return mpfr_get_d (self->res, MPFR_RNDN);
}

const gchar *
sums_eval_get_res_str (SumsEval *self)
{
  g_return_val_if_fail (SUMS_IS_EVAL (self), NULL);

  return self->res_str;
}

gboolean
sums_eval_parse (SumsEval     *self,
                 const gchar  *expr,
                 GError      **error)
{
  g_autofree const gchar *normalised = NULL;
  g_auto (GStrv) tokens_strv = NULL;
  guint len;
  mpfr_t op;
  g_autoptr (GError) local_error = NULL;

  g_return_val_if_fail (SUMS_IS_EVAL (self), FALSE);
  g_return_val_if_fail (expr, FALSE);

  if (!strlen (expr))
    return FALSE;

  add_to_history (self, expr);

  normalised = get_normalised (expr);
  tokens_strv = g_strsplit (normalised, " ", -1);
  len = g_strv_length (tokens_strv);
  mpfr_init (op);

  for (guint i = 0; i < len; i++)
    {
      if (!strlen (tokens_strv[i]))
        continue;

      if (is_number (tokens_strv[i]))
        {
          mpfr_set_str (op, tokens_strv[i], 0, MPFR_RNDN);
          push (self, op);
          continue;
        }

      switch (lookup_token (tokens_strv[i]))
        {
        case TOKEN_ADD:
          add (self, op, &local_error);
          break;

        case TOKEN_ANS:
          push_ans (self, &local_error);
          break;

        case TOKEN_COS:
          cosine (self, op, &local_error);
          break;

        case TOKEN_DIV:
          divide (self, op, &local_error);
          break;

        case TOKEN_E:
          push_e (self, op);
          break;

        case TOKEN_FAC:
          fac (self, op, &local_error);
          break;

        case TOKEN_LOG:
          logarithm (self, op, LOG_BASE_E, &local_error);
          break;

        case TOKEN_LOG10:
          logarithm (self, op, LOG_BASE_10, &local_error);
          break;

        case TOKEN_LOG2:
          logarithm (self, op, LOG_BASE_2, &local_error);
          break;

        case TOKEN_MOD:
          mod (self, op, &local_error);
          break;

        case TOKEN_MULT:
          mult (self, op, &local_error);
          break;

        case TOKEN_PI:
          push_pi (self, op);
          break;

        case TOKEN_POW:
          power (self, op, &local_error);
          break;

        case TOKEN_SIN:
          sine (self, op, &local_error);
          break;

        case TOKEN_SQRT:
          sqroot (self, op, &local_error);
          break;

        case TOKEN_SUB:
          sub (self, op, &local_error);
          break;

        case TOKEN_SUM:
          sum (self, op, &local_error);
          break;

        case TOKEN_TAN:
          tangent (self, op, &local_error);
          break;

        default:
          g_set_error (error,
                       SUMS_EVAL_ERROR,
                       SUMS_EVAL_ERROR_INVALID_TOKEN,
                       "Invalid expression");
          goto out;
        }

      if (local_error)
        {
          g_propagate_error (error, g_steal_pointer (&local_error));
          goto out;
        }
    }

  if (self->stack_pos > 1)
    {
      g_set_error (error,
                   SUMS_EVAL_ERROR,
                   SUMS_EVAL_ERROR_IMBALANCED_EXPRESSION,
                   "Incomplete expression");
      goto out;
    }

  mpfr_swap (self->res, pop (self));
  set_res_str (self);

out:
  mpfr_clear (op);
  g_array_remove_range (self->stack, 0, self->stack->len);
  self->stack_pos = 0;

  return TRUE;
}

void
sums_eval_queue_flush (SumsEval *self)
{
  g_return_if_fail (SUMS_IS_EVAL (self));

  self->flush_set = TRUE;
}

SumsEval *
sums_eval_new (void)
{
  return g_object_new (SUMS_TYPE_EVAL, NULL);
}

static void
sums_eval_finalize (GObject *obj)
{
  SumsEval *self = SUMS_EVAL (obj);

  mpfr_clear (self->res);
  g_free (self->res_str);
  g_array_free (self->stack, TRUE);
  g_clear_list (&self->history, (GDestroyNotify) g_free);

  G_OBJECT_CLASS (sums_eval_parent_class)->finalize (obj);
}

static void
sums_eval_set_property (GObject      *object,
                        guint         prop_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
  SumsEval *self = SUMS_EVAL (object);

  switch (prop_id)
    {
    case PROP_ANGULAR_UNIT:
      self->angular_unit = g_value_get_enum (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_eval_get_property (GObject    *object,
                        guint       prop_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
  SumsEval *self = SUMS_EVAL (object);

  switch (prop_id)
    {
    case PROP_ANGULAR_UNIT:
      g_value_set_enum (value, self->angular_unit);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_eval_class_init (SumsEvalClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);

  obj_class->get_property = sums_eval_get_property;
  obj_class->set_property = sums_eval_set_property;
  obj_class->finalize = sums_eval_finalize;

  props[PROP_ANGULAR_UNIT] = g_param_spec_enum ("angular-unit", NULL, NULL,
                                                SUMS_TYPE_EVAL_ANGULAR_UNIT,
                                                SUMS_EVAL_ANGULAR_UNIT_RADIAN,
                                                G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (obj_class, N_PROPS, props);
}

static void
sums_eval_init (SumsEval *self)
{
  self->stack = g_array_new (FALSE, FALSE, sizeof (mpfr_t));

  mpfr_init (self->res);
  g_array_set_clear_func (self->stack, (GDestroyNotify) mpfr_clear);
}
