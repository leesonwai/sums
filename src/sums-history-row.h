/* sums-history-row.h
 *
 * Copyright 2020-2021 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SUMS_TYPE_HISTORY_ROW (sums_history_row_get_type ())

G_DECLARE_FINAL_TYPE (SumsHistoryRow, sums_history_row, SUMS, HISTORY_ROW, AdwActionRow)

const gchar *       sums_history_row_get_expr       (SumsHistoryRow *self);

void                sums_history_row_set_expr       (SumsHistoryRow *self,
                                                     const gchar    *expr);

const gchar *       sums_history_row_get_res        (SumsHistoryRow *self);

void                sums_history_row_set_res        (SumsHistoryRow *self,
                                                     const gchar    *res);

SumsHistoryRow *    sums_history_row_new            (void);

G_END_DECLS
