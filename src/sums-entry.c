/* sums-entry.c
 *
 * Copyright 2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "sums-entry.h"
#include "sums-eval.h"

struct _SumsEntry
{
  GtkTextView            parent;

  SumsEval              *eval;
  GtkTextBuffer         *buffer;
  GtkEventControllerKey *ctrl;

  gchar                 *expr;
  gchar                 *error;

  GSettings             *settings;
};

enum
{
  PROP_0,
  PROP_EXPR,
  PROP_EVAL,
  PROP_ERROR,

  N_PROPS
};

G_DEFINE_FINAL_TYPE (SumsEntry, sums_entry, GTK_TYPE_TEXT_VIEW)

static GParamSpec *props[N_PROPS];

static void
set_error (SumsEntry   *self,
           const gchar *error)
{
  g_assert (SUMS_IS_ENTRY (self));

  g_free (self->error);

  self->error = g_strdup (error);

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_ERROR]);
}

static void
backward_token_start (GtkTextIter *iter)
{
  if (!gtk_text_iter_is_start (iter))
    gtk_text_iter_backward_char (iter);

  while (!gtk_text_iter_is_start (iter) && !g_unichar_isspace (gtk_text_iter_get_char (iter)))
    gtk_text_iter_backward_char (iter);

  if (g_unichar_isspace (gtk_text_iter_get_char (iter)))
    gtk_text_iter_forward_char (iter);
}

static void
set_expr (SumsEntry   *self,
          const gchar *expr)
{
  g_assert (SUMS_IS_ENTRY (self));
  g_assert (*expr || expr);

  g_free (self->expr);

  self->expr = g_strdup (expr);
  g_strstrip (self->expr);

  g_object_notify_by_pspec (G_OBJECT (self), props[PROP_EXPR]);
}

static void
submit_expr (SumsEntry *self)
{
  g_autofree const gchar *expr = NULL;
  g_autoptr (GError) error = NULL;

  expr = sums_entry_get_text (self);
  if (sums_eval_parse (self->eval, expr, &error) && !error)
    {
      set_expr (self, expr);
      sums_entry_set_text (self, "");
    }
  else
    {
      set_error (self, error->message);
    }
}

void
sums_entry_set_text (SumsEntry   *self,
                     const gchar *text)
{
  g_return_if_fail (SUMS_IS_ENTRY (self));
  g_return_if_fail (text || *text);

  if (!g_strcmp0 (sums_entry_get_text (self), text))
    return;

  gtk_text_buffer_set_text (self->buffer, text, -1);
}

const gchar *
sums_entry_get_text (SumsEntry *self)
{
  GtkTextIter start, end;

  g_return_val_if_fail (SUMS_IS_ENTRY (self), NULL);

  gtk_text_buffer_get_bounds (self->buffer, &start, &end);

  return gtk_text_buffer_get_text (self->buffer, &start, &end, FALSE);
}

const gchar *
sums_entry_get_expr (SumsEntry *self)
{
  g_return_val_if_fail (SUMS_IS_ENTRY (self), NULL);

  return self->expr;
}

const gchar *
sums_entry_get_error (SumsEntry *self)
{
  g_return_val_if_fail (SUMS_IS_ENTRY (self), NULL);

  return self->error;
}

static gboolean
on_key_pressed (GtkEventControllerKey *controller,
                guint                  keyval,
                guint                  keycode,
                GdkModifierType        state,
                gpointer               user_data)
{
  const gchar *history;
  SumsEntry *self = SUMS_ENTRY (user_data);

  g_assert (SUMS_IS_ENTRY (self));

  switch (keyval)
    {
    case GDK_KEY_Down:
      history = sums_eval_get_next_history (self->eval);
      if (history)
        sums_entry_set_text (self, history);
      else
        sums_entry_set_text (self, "");
      break;

    case GDK_KEY_Return:
      submit_expr (self);
      break;

    case GDK_KEY_Up:
      history = sums_eval_get_prev_history (self->eval);
      if (history)
        sums_entry_set_text (self, history);
      break;

    default:
      return GDK_EVENT_PROPAGATE;
    }

  return GDK_EVENT_STOP;
}

static void
on_buffer_changed (SumsEntry *self)
{
  GtkTextIter end, start;
  g_autofree const gchar *token = NULL;
  const gchar *symbol;

  g_assert (SUMS_IS_ENTRY (self));

  gtk_text_buffer_get_iter_at_mark (self->buffer,
                                    &end,
                                    gtk_text_buffer_get_mark (self->buffer,
                                                              "insert"));

  start = end;
  backward_token_start (&start);

  token = gtk_text_buffer_get_text (self->buffer, &start, &end, FALSE);
  symbol = sums_eval_convert (self->eval, token);

  if (symbol)
    {
      g_signal_handlers_block_by_func (self->buffer, on_buffer_changed, self);
      gtk_text_buffer_delete (self->buffer, &start, &end);
      gtk_text_buffer_insert_at_cursor (self->buffer, symbol, -1);
      g_signal_handlers_unblock_by_func (self->buffer, on_buffer_changed, self);
    }
}

SumsEntry *
sums_entry_new (void)
{
  return g_object_new (SUMS_TYPE_ENTRY, NULL);
}

static void
sums_entry_finalize (GObject *obj)
{
  SumsEntry *self = SUMS_ENTRY (obj);

  g_free (self->expr);
  g_free (self->error);
  g_clear_object (&self->settings);

  G_OBJECT_CLASS (sums_entry_parent_class)->finalize (obj);
}

static void
sums_entry_dispose (GObject *obj)
{
  SumsEntry *self = SUMS_ENTRY (obj);

  gtk_widget_dispose_template (GTK_WIDGET (self), SUMS_TYPE_ENTRY);

  G_OBJECT_CLASS (sums_entry_parent_class)->dispose (obj);
}

static void
sums_entry_set_property (GObject      *object,
                         guint         prop_id,
                         const GValue *value,
                         GParamSpec   *pspec)
{
  SumsEntry *self = SUMS_ENTRY (object);

  switch (prop_id)
    {
    case PROP_EVAL:
      self->eval = g_value_get_object (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_entry_get_property (GObject    *object,
                         guint       prop_id,
                         GValue     *value,
                         GParamSpec *pspec)
{
  SumsEntry *self = SUMS_ENTRY (object);

  switch (prop_id)
    {
    case PROP_EXPR:
      g_value_set_string (value, sums_entry_get_expr (self));
      break;

    case PROP_ERROR:
      g_value_set_string (value, sums_entry_get_error (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
sums_entry_class_init (SumsEntryClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  obj_class->get_property = sums_entry_get_property;
  obj_class->set_property = sums_entry_set_property;
  obj_class->dispose = sums_entry_dispose;
  obj_class->finalize = sums_entry_finalize;

  props[PROP_EXPR] = g_param_spec_string ("expr", NULL, NULL, NULL,
                                           G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
  props[PROP_EVAL] = g_param_spec_object ("eval", NULL, NULL, SUMS_TYPE_EVAL,
                                          G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY | G_PARAM_STATIC_STRINGS);
  props[PROP_ERROR] = g_param_spec_string ("error", NULL, NULL, NULL,
                                           G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);
  g_object_class_install_properties (obj_class, N_PROPS, props);

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Sums/sums-entry.ui");

  gtk_widget_class_bind_template_child (widget_class, SumsEntry, buffer);

  gtk_widget_class_bind_template_callback (widget_class, on_buffer_changed);
}

static void
sums_entry_init (SumsEntry *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->ctrl = GTK_EVENT_CONTROLLER_KEY (gtk_event_controller_key_new ());
  self->settings = g_settings_new ("io.gitlab.leesonwai.Sums");

  gtk_widget_add_controller (GTK_WIDGET (self), GTK_EVENT_CONTROLLER (self->ctrl));

  g_signal_connect (self->ctrl, "key-pressed",
                    G_CALLBACK (on_key_pressed), self);
}
