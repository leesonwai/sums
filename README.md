# Sums

<a href="https://flathub.org/apps/details/io.gitlab.leesonwai.Sums"><img src="https://flathub.org/assets/badges/flathub-badge-en.png" width="256px"/></a>

Sums is a simple GTK postfix calculator that adheres to GNOME's human-interface
guidelines. It is designed to be keyboard-driven and aims to feel natural to
interact with by recognising English-language input as mathematical constants
and operations.

For example, the expression `1 2 pi 4 5 sum` will return the result
`15.1415926`. Expressions will also be converted as you type to display
mathematical symbols where possible. The previous example would be displayed as
`1 2 π 4 5 Σ`.

![alt text](/data/screenshots/sums-busy.png "Sums")

## Building

The recommended way to build Sums is with GNOME Builder.

#### Dependencies

* `gtk4 >= 4.8.0`
* `libadwaita-1 >= 1.4.0`
* `mpfr`
