/* sums-window.c
 *
 * Copyright 2020-2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"
#include "sums-window.h"
#include "sums-view.h"

struct _SumsWindow
{
  AdwApplicationWindow   parent;

  GtkButton             *clear_button;
  SumsView              *view;

  GSettings             *settings;
};

G_DEFINE_FINAL_TYPE (SumsWindow, sums_window, ADW_TYPE_APPLICATION_WINDOW)

static void
on_clear_button_clicked (SumsWindow *self)
{
  g_assert (SUMS_IS_WINDOW (self));

  sums_view_clear_history (self->view);
}

static void
clear_history_activated (GtkWidget   *widget,
                         const gchar *action_name,
                         GVariant    *parameter)
{
  SumsWindow *self = SUMS_WINDOW (widget);

  sums_view_clear_history (self->view);
}

SumsWindow *
sums_window_new (SumsApplication *app)
{
  return g_object_new (SUMS_TYPE_WINDOW, "application", app, NULL);
}

static void
sums_window_constructed (GObject *obj)
{
  SumsWindow *self = SUMS_WINDOW (obj);

#if DEVEL_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

  if (g_settings_get_boolean (self->settings, "window-maximized"))
    gtk_window_maximize (GTK_WINDOW (self));

  G_OBJECT_CLASS (sums_window_parent_class)->constructed (obj);
}

static void
sums_window_finalize (GObject *obj)
{
  SumsWindow *self = SUMS_WINDOW (obj);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (sums_window_parent_class)->finalize (obj);
}

static void
sums_window_dispose (GObject *obj)
{
  SumsWindow *self = SUMS_WINDOW (obj);

  gtk_widget_dispose_template (GTK_WIDGET (self), SUMS_TYPE_WINDOW);

  G_OBJECT_CLASS (sums_window_parent_class)->dispose (obj);
}

static void
sums_window_class_init (SumsWindowClass *klass)
{
  GObjectClass *obj_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  obj_class->constructed = sums_window_constructed;
  obj_class->dispose = sums_window_dispose;
  obj_class->finalize = sums_window_finalize;

  gtk_widget_class_set_template_from_resource (widget_class, "/io/gitlab/leesonwai/Sums/sums-window.ui");

  gtk_widget_class_install_action (widget_class, "win.clear-history", NULL, clear_history_activated);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_c, GDK_SHIFT_MASK | GDK_CONTROL_MASK, "win.clear-history", NULL);

  gtk_widget_class_bind_template_child (widget_class, SumsWindow, clear_button);
  gtk_widget_class_bind_template_child (widget_class, SumsWindow, view);

  gtk_widget_class_bind_template_callback (widget_class, on_clear_button_clicked);

  g_type_ensure (SUMS_TYPE_VIEW);
}

static void
sums_window_init (SumsWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

  self->settings = g_settings_new ("io.gitlab.leesonwai.Sums");

  g_settings_bind (self->settings, "window-height",
                   self, "default-height",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "window-maximized",
                   self, "maximized",
                   G_SETTINGS_BIND_DEFAULT);

  g_settings_bind (self->settings, "window-width",
                   self, "default-width",
                   G_SETTINGS_BIND_DEFAULT);
}
