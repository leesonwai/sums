/* sums-entry.h
 *
 * Copyright 2024 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

#define SUMS_TYPE_ENTRY (sums_entry_get_type ())

G_DECLARE_FINAL_TYPE (SumsEntry, sums_entry, SUMS, ENTRY, GtkTextView)

const gchar *       sums_entry_get_error            (SumsEntry *self);

const gchar *       sums_entry_get_expr             (SumsEntry *self);

const gchar *       sums_entry_get_text             (SumsEntry *self);

void                sums_entry_set_text             (SumsEntry   *self,
                                                     const gchar *text);

SumsEntry *         sums_entry_new                  (void);

G_END_DECLS
